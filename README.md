# Library App

Web application used to store books in the library. When the user is not logged in, they can only view and search for books. A logged-in user has all the functionality of a non-logged-in user, but can also borrow and return books, view a list of active borrows, change a password, or delete an account. An administrator can view all active borrowings of users, search for books, add them and edit them.

**Technology used:** Java EE, Spring, Spring Boot, Hibernate, Jpa, Maven, Vaadin, MySQL


### **Unauthorized view:** ![Unauthorized_view](/uploads/cf0fccfce0231590abb3b9df8d053bc3/Unauthorized_view.png)

### **Admin view:** ![Admin_view](/uploads/479f2d1867a43aed643ab19245175aa9/Admin_view.png)

### **User view:** ![User_view](/uploads/6c4ed3c7649b6262e5a605da0dc71a73/User_view.png)

### **Log in:** ![Log_in_view](/uploads/e0050165c00c0a802f5c495b99617ba2/Log_in_view.png)

### **Sign up:** ![Sign_up_view](/uploads/af128a0a45191e23fa9919bf02627ad0/Sign_up_view.png)

