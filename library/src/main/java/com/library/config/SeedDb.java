package com.library.config;

import com.library.data.entity.BookEntity;
import com.library.data.entity.UserEntity;
import com.library.service.BookService;
import com.library.service.UserService;
import jakarta.annotation.PostConstruct;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SeedDb {

    private final UserService userService;
    private final BookService bookService;

    @Autowired
    public SeedDb(UserService userService, BookService bookService) {
        this.userService = userService;
        this.bookService = bookService;
    }

    /**
     * Seeds the database with some default books and inject admin user.
     */
    @PostConstruct
    public void init() {
        List<BookEntity> booksToSeed = List.of(
                new BookEntity("To Kill a Mockingbird", "Harper Lee", "Fiction"),
                new BookEntity("1984", "George Orwell", "Dystopian"),
                new BookEntity("The Great Gatsby", "F. Scott Fitzgerald", "Classic"),
                new BookEntity("The Catcher in the Rye", "J.D. Salinger", "Classic"),
                new BookEntity("Pride and Prejudice", "Jane Austen", "Romance"),
                new BookEntity("The Lord of the Rings", "J.R.R. Tolkien", "Fantasy"),
                new BookEntity("The Hobbit", "J.R.R. Tolkien", "Fantasy"),
                new BookEntity("Harry Potter and the Sorcerer's Stone", "J.K. Rowling", "Fantasy"),
                new BookEntity("Moby Dick", "Herman Melville", "Adventure"),
                new BookEntity("War and Peace", "Leo Tolstoy", "Historical"),
                new BookEntity("Crime and Punishment", "Fyodor Dostoevsky", "Psychological Fiction"),
                new BookEntity("The Brothers Karamazov", "Fyodor Dostoevsky", "Philosophical Fiction"),
                new BookEntity("Jane Eyre", "Charlotte Brontë", "Gothic Fiction"),
                new BookEntity("Wuthering Heights", "Emily Brontë", "Gothic Fiction"),
                new BookEntity("Brave New World", "Aldous Huxley", "Dystopian"),
                new BookEntity("The Odyssey", "Homer", "Epic Poetry"),
                new BookEntity("The Iliad", "Homer", "Epic Poetry"),
                new BookEntity("Don Quixote", "Miguel de Cervantes", "Adventure"),
                new BookEntity("Ulysses", "James Joyce", "Modernist"),
                new BookEntity("One Hundred Years of Solitude", "Gabriel García Márquez", "Magic Realism")
        );

        for (BookEntity book : booksToSeed) {
            List<BookEntity> existingBooks = bookService.getBooksByTitle(book.getTitle());
            if (existingBooks.isEmpty()) {
                bookService.saveBook(book);
            }
        }


        UserEntity admin = userService.findByUsername("admin");
        if (admin == null) {
            admin = new UserEntity();
            admin.setUsername("admin");
            String hashedPassword = BCrypt.hashpw("admin", BCrypt.gensalt());
            admin.setPassword(hashedPassword);
            admin.setRole("ADMIN");
            userService.saveUser(admin);
        }


    }
}
