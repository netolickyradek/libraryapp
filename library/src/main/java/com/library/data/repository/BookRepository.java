package com.library.data.repository;

import com.library.data.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for accessing and managing book entities in the database.
 */
@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long> {}