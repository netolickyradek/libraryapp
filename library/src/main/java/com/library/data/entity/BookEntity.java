package com.library.data.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "BOOK")
public class BookEntity {

    public BookEntity(String title, String author, String genre) {
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.isBorrowed = false;
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "AUTHOR")
    private String author;

    @Column(name = "GENRE")
    private String genre;

    @Column(name = "IS_BORROWED")
    private boolean isBorrowed;

    @Column(name = "BORROW_DATE")
    private LocalDate borrowDate;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity borrowedBy;

    /**
     * Calculates the return date for a borrowed book.
     *
     * @return The return date, which is one month after the borrow date, or null if the book hasn't been borrowed yet.
     */
    public LocalDate getReturnDate() {
        return borrowDate != null ? borrowDate.plusMonths(1) : null;
    }
}
