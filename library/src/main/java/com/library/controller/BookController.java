package com.library.controller;

import com.library.data.entity.BookEntity;
import com.library.data.entity.UserEntity;
import com.library.service.BookService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.library.service.AuthenticationService.getCurrentUser;

@RestController
@RequestMapping("/library")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    /**
     * Retrieves a list of all books available in the library.
     *
     * @return A list of BookEntity objects representing all books in the library.
     */
    @GetMapping
    public List<BookEntity> getAllBooks() {
        return bookService.getAllBooks();
    }

    /**
     * Retrieves a book from the library by its unique identifier (ID).
     *
     * @param id The unique identifier of the book to retrieve.
     * @return The BookEntity object representing the book with the specified ID.
     */
    @GetMapping("/{id}")
    public BookEntity getBookById(@PathVariable Long id) {
        return bookService.getBookById(id);
    }

    /**
     * Creates a new book in the library.
     *
     * @param book The BookEntity object representing the book to be created.
     * @return The BookEntity object representing the newly created book.
     * @throws RuntimeException If the current user is not authorized to create a book.
     */
    @PostMapping
    public BookEntity createBook(@RequestBody BookEntity book) {
        UserEntity currentUser = getCurrentUser();
        if (currentUser != null && currentUser.getRole().equals("ADMIN")) {
            return bookService.saveBook(book);
        } else {
            throw new RuntimeException("User is not authorized to create a book");
        }
    }

    /**
     * Updates an existing book in the library.
     *
     * @param id   The unique identifier of the book to be updated.
     * @param book The BookEntity object representing the updated information of the book.
     * @return The BookEntity object representing the updated book.
     */
    @PutMapping("/{id}")
    public BookEntity updateBook(@PathVariable Long id, @RequestBody BookEntity book) {
        book.setId(id);
        return bookService.saveBook(book);
    }

    /**
     * Deletes a book from the library.
     *
     * @param id The unique identifier of the book to be deleted.
     */
    @DeleteMapping("/{id}")
    public void deleteBook(@PathVariable Long id) {
        bookService.deleteBook(id);
    }

    /**
     * Borrows a book from the library.
     *
     * @param id The unique identifier of the book to be borrowed.
     * @return A message indicating the result of the borrowing attempt.
     * @throws RuntimeException If the current user is not authorized to borrow a book.
     */
    @PostMapping("/{id}/borrow")
    public String borrowBook(@PathVariable Long id) {
        UserEntity currentUser = getCurrentUser();
        if (currentUser != null && currentUser.getRole().equals("USER")) {
            BookEntity book = bookService.getBookById(id);
            if (bookService.borrowBook(currentUser, book)) {
                return "Book borrowed successfully.";
            } else {
                return "Book is already borrowed.";
            }
        } else {
            throw new RuntimeException("User is not authorized to borrow a book");
        }
    }

    /**
     * Returns a borrowed book to the library.
     *
     * @param id The unique identifier of the book to be returned.
     * @return A message indicating the result of the return attempt.
     * @throws RuntimeException If the current user is not authorized to return a book.
     */
    @PostMapping("/{id}/return")
    public String returnBook(@PathVariable Long id) {
        UserEntity currentUser = getCurrentUser();
        if (currentUser != null && currentUser.getRole().equals("USER")) {
            BookEntity book = bookService.getBookById(id);
            if (bookService.returnBook(currentUser, book)) {
                return "Book returned successfully.";
            } else {
                return "Book was not borrowed.";
            }
        } else {
            throw new RuntimeException("User is not authorized to return a book");
        }
    }
}
