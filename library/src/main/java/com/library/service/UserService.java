package com.library.service;

import com.library.data.entity.BookEntity;
import com.library.data.entity.UserEntity;
import com.library.data.repository.UserRepository;
import com.vaadin.flow.component.notification.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.mindrot.jbcrypt.BCrypt;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final BookService bookService;

    @Autowired
    public UserService(UserRepository userRepository, BookService bookService) {
        this.userRepository = userRepository;
        this.bookService = bookService;
    }

    /**
     * Retrieves a list of all users registered in the library.
     *
     * @return A list of UserEntity objects representing all users in the library.
     */
    public UserEntity registerUser(String username, String password) {
        if (userRepository.findByUsername(username) != null) {
            return null;
        }
        UserEntity user = new UserEntity();
        user.setUsername(username);
        String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());
        user.setPassword(hashedPassword);
        user.setRole("USER");
        return userRepository.save(user);
    }

    /**
     * Logs in a user with the specified username and password.
     *
     * @param username The username of the user to log in.
     * @param password The password of the user to log in.
     * @return The UserEntity object representing the logged-in user.
     */
    public UserEntity loginUser(String username, String password) {
        UserEntity user = userRepository.findByUsername(username);
        if (user != null && BCrypt.checkpw(password, user.getPassword())) {
            return user;
        } else return null;
    }

    /**
     * Retrieves a user from the library by their username.
     *
     * @param username The username of the user to retrieve.
     * @return The UserEntity object representing the user with the specified username.
     */
    public UserEntity findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    /**
     * Saves a user to the library.
     *
     * @param user The UserEntity object representing the user to save.
     */
    public void saveUser(UserEntity user) {
        userRepository.save(user);
    }

    /**
     * Deletes a user from the library.
     *
     * @param user The UserEntity object representing the user to delete.
     */
    public void deleteUser(UserEntity user) {
        List<BookEntity> activeBorrowings = bookService.getActiveBorrowings(user);
        if (activeBorrowings.isEmpty()) {
            // Pokud uživatel nemá žádné aktivní výpůjčky, provede se smazání uživatele
            userRepository.delete(user);
        } else {
            Notification.show("User has active borrowings, cannot delete user");
            throw new IllegalStateException("User has active borrowings, cannot delete user");
        }
    }
}
