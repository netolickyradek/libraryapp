package com.library.service;

import com.library.data.entity.UserEntity;
import com.vaadin.flow.server.VaadinSession;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {
    private final UserService userService;

    @Autowired
    public AuthenticationService(UserService userService) {
        this.userService = userService;
    }

    /**
     * Retrieves the currently logged-in user from the current session using Vaadin.
     *
     * @return The UserEntity object representing the currently logged-in user, or null if no user is logged in.
     */
    public static UserEntity getCurrentUser() {
        return VaadinSession.getCurrent().getAttribute(UserEntity.class);
    }

    /**
     * Logs out the currently logged-in user by invalidating the current Vaadin session.
     * After logout, the user will be redirected to the main page or will need to re-authenticate.
     */
    public void logout() {
        VaadinSession.getCurrent().getSession().invalidate();
    }

    /**
     * Checks if the provided password matches the hashed password of the given user.
     *
     * @param user     The UserEntity object representing the user.
     * @param password The plain-text password to validate.
     * @return True if the provided password matches the hashed password of the user, false otherwise.
     */
    public boolean isPasswordValid(UserEntity user, String password) {
        return !BCrypt.checkpw(password, user.getPassword());
    }

    /**
     * Changes the password of the given user to the provided new password.
     *
     * @param user        The UserEntity object representing the user.
     * @param newPassword The new plain-text password to set for the user.
     * @return True if the password was successfully changed, false otherwise.
     */
    public boolean changePassword(UserEntity user, String newPassword) {
        try {
            String hashedPassword = BCrypt.hashpw(newPassword, BCrypt.gensalt());
            user.setPassword(hashedPassword);
            userService.saveUser(user);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
