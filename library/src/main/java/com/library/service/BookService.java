package com.library.service;

import com.library.data.entity.BookEntity;
import com.library.data.entity.UserEntity;
import com.library.data.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class BookService {

    private final BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    /**
     * Retrieves a list of all books available in the library.
     *
     * @return A list of BookEntity objects representing all books in the library.
     */
    public List<BookEntity> getAllBooks() {
        return bookRepository.findAll();
    }

    /**
     * Retrieves a book from the library by its unique identifier (ID).
     *
     * @param id The unique identifier of the book to retrieve.
     * @return The BookEntity object representing the book with the specified ID.
     */
    public BookEntity getBookById(Long id) {
        return bookRepository.findById(id).orElse(null);
    }

    /**
     * Creates a new book in the library.
     *
     * @param book The BookEntity object representing the book to be created.
     * @return The BookEntity object representing the newly created book.
     */
    public BookEntity saveBook(BookEntity book) {
        return bookRepository.save(book);
    }

    /**
     * Deletes a book from the library by its unique identifier (ID).
     *
     * @param id The unique identifier of the book to delete.
     */
    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    /**
     * Retrieves a list of books from the library that match the specified title.
     *
     * @param title The title to search for.
     * @return A list of BookEntity objects representing books with titles that match the specified title.
     */
    public List<BookEntity> getBooksByTitle(String title) {
        List<BookEntity> allBooks = bookRepository.findAll();
        Pattern pattern = Pattern.compile(".*" + Pattern.quote(title) + ".*", Pattern.CASE_INSENSITIVE);
        return allBooks.stream()
                .filter(book -> pattern.matcher(book.getTitle()).matches())
                .collect(Collectors.toList());
    }

    /**
     * Borrows a book from the library.
     *
     * @param user The user borrowing the book.
     * @param book The book to be borrowed.
     * @return True if the book was successfully borrowed, false if the book is already borrowed.
     */
    public boolean borrowBook(UserEntity user, BookEntity book) {
        if (!book.isBorrowed()) {
            book.setBorrowedBy(user);
            book.setBorrowed(true);
            book.setBorrowDate(LocalDate.now());
            bookRepository.save(book);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns a borrowed book to the library.
     *
     * @param user The user returning the book.
     * @param book The book to be returned.
     * @return True if the book was successfully returned, false if the book is not borrowed by the specified user.
     */
    public boolean returnBook(UserEntity user, BookEntity book) {
        if (book.isBorrowed() && book.getBorrowedBy().equals(user)) {
            book.setBorrowedBy(null);
            book.setBorrowed(false);
            book.setBorrowDate(null);
            bookRepository.save(book);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Retrieves a list of books that are currently borrowed by the specified user.
     *
     * @param user The user to retrieve borrowed books for.
     * @return A list of BookEntity objects representing books that are currently borrowed by the specified user.
     */
    public List<BookEntity> getActiveBorrowings(UserEntity user) {
        return bookRepository.findAll().stream()
                .filter(book -> book.isBorrowed() && book.getBorrowedBy().equals(user))
                .collect(Collectors.toList());
    }

    /**
     * Retrieves a list of all books that are currently borrowed by users.
     *
     * @return A list of BookEntity objects representing all books that are currently borrowed.
     */
    public List<BookEntity> getAllActiveBorrowings() {
        return bookRepository.findAll().stream()
                .filter(BookEntity::isBorrowed)
                .collect(Collectors.toList());
    }
}
