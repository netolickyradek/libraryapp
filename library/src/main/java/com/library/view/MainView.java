package com.library.view;

import com.library.data.entity.BookEntity;
import com.library.data.entity.UserEntity;
import com.library.service.AuthenticationService;
import com.library.service.BookService;
import com.library.service.UserService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterListener;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.library.service.AuthenticationService.getCurrentUser;
import static com.library.view.RegisterView.isStrongPassword;

@Route("/library")
@Component
@Scope("prototype")
public class MainView extends VerticalLayout implements BeforeEnterListener {

    private final BookService bookService;
    private final UserService userService;
    private final AuthenticationService authenticationService;
    private final Grid<BookEntity> grid;
    private final TextField titleField;
    private final TextField authorField;
    private final TextField genreField;
    private final TextField searchBar;
    private BookEntity selectedBook;
    private final UserEntity currentUser;
    @Autowired
    public MainView(BookService bookService, AuthenticationService authenticationService, UserService userService) {
        this.bookService = bookService;
        this.authenticationService = authenticationService;
        this.userService = userService;

        // Initialize fields
        this.grid = new Grid<>(BookEntity.class);
        this.titleField = new TextField("Title");
        this.authorField = new TextField("Author");
        this.genreField = new TextField("Genre");

        // Create buttons
        Button saveButton = new Button("Save", event -> saveBook());

        Button registerButton = new Button("Sign up", event -> getUI().ifPresent(ui -> ui.navigate("/library/register")));
        registerButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        Button loginButton = new Button("Log in", event -> getUI().ifPresent(ui -> ui.navigate("/library/login")));
        loginButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        Button logoutButton = new Button("Log out", event -> logoutUser());
        logoutButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        Button borrowingsButton = new Button("My Borrowings", event -> showUserBorrowings());
        borrowingsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        Button allBorrowingsButton = new Button("Active Borrowings", event -> showAllBorrowings());
        allBorrowingsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        Button searchButton = new Button("Search", event -> searchBook());
        Button changePasswordButton = createChangePasswordButton();
        Button deleteAccountButton = createDeleteAccountButton();

        // Create search bar
        this.searchBar = new TextField();
        this.searchBar.setPlaceholder("Search by title");
        this.searchBar.setValueChangeMode(ValueChangeMode.EAGER);

        // Create icon
        Icon icon = VaadinIcon.BOOK.create();

        // Create form layout
        FormLayout formLayout = new FormLayout();
        formLayout.add(titleField, authorField, genreField, saveButton);

        // Create logo layout
        HorizontalLayout logoLayout = new HorizontalLayout(icon, new Span(new H3("Library Reservation System")));

        // Create button layout based on user role
        currentUser = VaadinSession.getCurrent().getAttribute(UserEntity.class);
        HorizontalLayout buttonLayout = new HorizontalLayout();
        if (currentUser == null) {
            buttonLayout.add(registerButton, loginButton);
        } else {
            buttonLayout.add(logoutButton);
            if (currentUser.getRole().equals("USER")) {
                buttonLayout.add(borrowingsButton, changePasswordButton, deleteAccountButton);
            } else if (currentUser.getRole().equals("ADMIN")) {
                buttonLayout.add(allBorrowingsButton);
            }
        }

        // Create main layout
        HorizontalLayout mainLayout = new HorizontalLayout(logoLayout, buttonLayout);
        mainLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        mainLayout.setWidthFull();

        // Create search layout
        HorizontalLayout searchLayout = new HorizontalLayout(searchBar, searchButton);

        // Add components to the view
        add(mainLayout, searchLayout, formLayout, grid);

        // Hide form for non-admin users
        if (currentUser == null || currentUser.getRole().equals("USER"))
            formLayout.setVisible(false);

        // Configure grid columns and actions
        grid.setColumns("id", "title", "author", "genre");
        if (currentUser != null) {
            grid.addComponentColumn(book -> {
                HorizontalLayout actionsLayout = new HorizontalLayout();
                if (currentUser.getRole().equals("ADMIN")) {
                    actionsLayout.add(createEditButton(book), createDeleteButton(book.getId()));
                } else if (currentUser.getRole().equals("USER")) {
                    actionsLayout.add(createBorrowButton(book));
                }
                return actionsLayout;
            }).setHeader("Actions");
        }

        // Add form and grid to the view
        add(formLayout, grid);

        // Set view properties
        setSizeFull();
        configureGrid();
        refreshGrid();
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if (VaadinSession.getCurrent().getAttribute(UserEntity.class) == null)
            event.rerouteTo("/login");
    }

    /**
     * Displays the user's borrowings in new dialog.
     */
    private void showUserBorrowings() {
        List<BookEntity> activeBorrowings = bookService.getActiveBorrowings(currentUser);

        Dialog dialog = new Dialog("My Borrowings");
        dialog.setWidth("1200px");
        dialog.setHeight("auto");
        Grid<BookEntity> borrowingsGrid = new Grid<>();
        borrowingsGrid.setItems(activeBorrowings);

        borrowingsGrid.removeAllColumns();
        borrowingsGrid.addColumn(BookEntity::getTitle).setHeader("Title");
        borrowingsGrid.addColumn(BookEntity::getAuthor).setHeader("Author");
        borrowingsGrid.addColumn(BookEntity::getGenre).setHeader("Genre");
        borrowingsGrid.addColumn(BookEntity::getBorrowDate).setHeader("Borrow Date");
        borrowingsGrid.addColumn(BookEntity::getReturnDate).setHeader("Return Date");

        borrowingsGrid.addComponentColumn(book -> createReturnButton(book, borrowingsGrid)).setHeader("Actions");

        createBorrowingsDialog(dialog, borrowingsGrid);
    }

    /**
     * Displays all active borrowings in new dialog, only for admin.
     */
    private void showAllBorrowings() {
        List<BookEntity> activeBorrowings = bookService.getAllActiveBorrowings();

        Dialog dialog = new Dialog("Active Borrowings");
        dialog.setWidth("1200px");
        dialog.setHeight("auto");
        Grid<BookEntity> borrowingsGrid = new Grid<>();
        borrowingsGrid.setItems(activeBorrowings);

        borrowingsGrid.removeAllColumns();
        borrowingsGrid.addColumn(book -> book.getBorrowedBy().getUsername()).setHeader("Username");
        borrowingsGrid.addColumn(BookEntity::getTitle).setHeader("Title");
        borrowingsGrid.addColumn(BookEntity::getAuthor).setHeader("Author");
        borrowingsGrid.addColumn(BookEntity::getBorrowDate).setHeader("Borrow Date");
        borrowingsGrid.addColumn(BookEntity::getReturnDate).setHeader("Return Date");

        createBorrowingsDialog(dialog, borrowingsGrid);
    }

    /**
     * Creates a dialog with the specified grid and opens it.
     *
     * @param dialog The dialog to add the grid to.
     * @param borrowingsGrid The grid to display in the dialog.
     */
    private void createBorrowingsDialog(Dialog dialog, Grid<BookEntity> borrowingsGrid) {
        HorizontalLayout headerLayout = new HorizontalLayout();
        headerLayout.setJustifyContentMode(JustifyContentMode.BETWEEN);
        headerLayout.setAlignItems(Alignment.CENTER);
        headerLayout.setWidthFull();

        Button closeButton = new Button(new Icon(VaadinIcon.CLOSE_SMALL), event -> dialog.close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_ICON);
        closeButton.getElement().setAttribute("aria-label", "Close");
        headerLayout.add(closeButton);

        VerticalLayout dialogLayout = new VerticalLayout(headerLayout, borrowingsGrid);
        dialog.add(dialogLayout);
        dialog.open();
    }

    /**
     * Configures the grid properties.
     */
    private void configureGrid() {
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setHeightFull();
        grid.asSingleSelect().addValueChangeListener(event -> {
            selectedBook = event.getValue();
            if (selectedBook != null)
                fillForm(selectedBook);
            else clearForm();

        });
    }

    /**
     * Creates a button for deleting an account.
     *
     * @return The created button.
     */
    private Button createDeleteAccountButton() {
        Button deleteAccountButton = new Button("Delete an account", clickEvent -> {
            Dialog deleteAccountDialog = new Dialog("Delete Account");
            deleteAccountDialog.setWidth("400px");
            deleteAccountDialog.setHeight("auto");

            PasswordField passwordField = new PasswordField("Password");
            VerticalLayout dialogLayout = getDeleteLayout(passwordField, deleteAccountDialog);
            dialogLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
            dialogLayout.setAlignItems(FlexComponent.Alignment.CENTER);
            deleteAccountDialog.add(dialogLayout);
            deleteAccountDialog.open();
        });

        deleteAccountButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);

        return deleteAccountButton;
    }

    /**
     * Creates a layout for deleting an account.
     *
     * @param passwordField The password field for the user to enter their password.
     * @param deleteAccountDialog The dialog to add the layout to.
     * @return The created layout.
     */
    private VerticalLayout getDeleteLayout(PasswordField passwordField, Dialog deleteAccountDialog) {
        Button deleteButton = new Button("Delete", event -> {
            String password = passwordField.getValue();

            if (authenticationService.isPasswordValid(currentUser, password)) {
                Notification.show("Password is incorrect");
                return;
            }

            userService.deleteUser(currentUser);
            authenticationService.logout();
            VaadinSession.getCurrent().setAttribute(UserEntity.class, null);
            getUI().ifPresent(ui -> ui.navigate("login"));
            Notification.show("Account deleted successfully");
            deleteAccountDialog.close();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        return new VerticalLayout(passwordField, deleteButton);
    }

    /**
     * Creates a button for changing the user's password.
     *
     * @return The created button.
     */
    private Button createChangePasswordButton() {
        Button changePasswordButton = new Button("Change Password", clickEvent -> {
            Dialog changePasswordDialog = new Dialog("Change Password");
            changePasswordDialog.setWidth("400px");
            changePasswordDialog.setHeight("auto");

            PasswordField currentPasswordField = new PasswordField("Current Password");
            PasswordField newPasswordField = new PasswordField("New Password");
            PasswordField confirmNewPasswordField = new PasswordField("Confirm New Password");

            Button saveButton = new Button("Save", event -> {
                String currentPassword = currentPasswordField.getValue();
                String newPassword = newPasswordField.getValue();
                String confirmNewPassword = confirmNewPasswordField.getValue();

                if (authenticationService.isPasswordValid(currentUser, currentPassword)) {
                    Notification.show("Current password is incorrect");
                    return;
                }

                if (!newPassword.equals(confirmNewPassword)) {
                    Notification.show("New passwords do not match");
                    return;
                }

                if (!isStrongPassword(newPassword) && !isStrongPassword(confirmNewPassword)) {
                    Notification.show("Password must be at least 6 characters long, contain at least one uppercase letter and one number");
                    return;
                }

                if (Objects.equals(currentPassword, newPassword)) {
                    Notification.show("New password cannot be the same as the current password");
                    return;
                }

                if (authenticationService.changePassword(currentUser, newPassword)) {
                    Notification.show("Password changed successfully");
                    changePasswordDialog.close();
                    //VaadinSession.getCurrent().getSession().invalidate();
                    VaadinSession.getCurrent().setAttribute(UserEntity.class, null);
                    getUI().ifPresent(ui -> ui.navigate("/library/login"));
                } else {
                    Notification.show("Password change failed");
                }
            });

            VerticalLayout dialogLayout = new VerticalLayout(currentPasswordField, newPasswordField, confirmNewPasswordField, saveButton);
            dialogLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
            dialogLayout.setAlignItems(FlexComponent.Alignment.CENTER);
            changePasswordDialog.add(dialogLayout);
            changePasswordDialog.open();
        });

        changePasswordButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        return changePasswordButton;
    }

    /**
     * Creates a button for editing a book.
     *
     * @param book The book to edit.
     * @return The created button.
     */
    private Button createEditButton(BookEntity book) {
        return new Button("Edit", clickEvent -> {
            selectedBook = book;
            fillForm(selectedBook);
        });
    }

    /**
     * Creates a button for deleting a book.
     *
     * @param bookId The ID of the book to delete.
     * @return The created button.
     */
    private Button createDeleteButton(Long bookId) {
        return new Button("Delete", clickEvent -> {
            bookService.deleteBook(bookId);
            refreshGrid();
            Notification.show("Book deleted");
            clearForm();
        });
    }

    /**
     * Creates a button for borrowing a book.
     *
     * @param book The book to borrow.
     * @return The created button.
     */
    private Button createBorrowButton(BookEntity book) {
        return new Button("Borrow", clickEvent -> {
            UserEntity currentUser1 = getCurrentUser();
            if (currentUser1 != null && currentUser1.getRole().equals("USER")) {
                if (bookService.borrowBook(currentUser1, book)) {
                    Notification.show("Book borrowed successfully.");
                    refreshGrid();
                } else Notification.show("Book is already borrowed.");
            } else Notification.show("Please make sure you are logged in as a user.");
        });
    }

    /**
     * Creates a button for returning a book.
     *
     * @param book The book to return.
     * @param borrowingsGrid The grid to update after returning the book.
     * @return The created button.
     */
    private Button createReturnButton(BookEntity book, Grid<BookEntity> borrowingsGrid) {
        return new Button("Return", clickEvent -> {
            UserEntity currentUser1 = getCurrentUser();
            if (currentUser1 != null && currentUser1.getRole().equals("USER")) {
                if (bookService.returnBook(currentUser1, book)) {
                    Notification.show("Book returned successfully.");
                    List<BookEntity> updatedBorrowings = bookService.getActiveBorrowings(currentUser1);
                    borrowingsGrid.setItems(updatedBorrowings);
                    refreshGrid();
                } else Notification.show("Book was not borrowed.");
            } else Notification.show("Please make sure you are logged in as a user.");
        });
    }

    /**
     * Refreshes the grid with available books.
     */
    private void refreshGrid() {
        List<BookEntity> allBooks = bookService.getAllBooks();
        List<BookEntity> availableBooks = allBooks.stream()
                .filter(book -> !book.isBorrowed())
                .collect(Collectors.toList());
        grid.setItems(availableBooks);
    }

    /**
     * Fills the form with the specified book's data.
     *
     * @param book The book to fill the form with.
     */
    private void fillForm(BookEntity book) {
        titleField.setValue(book.getTitle());
        authorField.setValue(book.getAuthor());
        genreField.setValue(book.getGenre());
    }

    /**
     * Clears the form.
     */
    private void clearForm() {
        selectedBook = null;
        titleField.clear();
        authorField.clear();
        genreField.clear();
    }

    /**
     * Saves the book data to the database.
     */
    private void saveBook() {
        if (selectedBook != null) {
            selectedBook.setTitle(titleField.getValue());
            selectedBook.setAuthor(authorField.getValue());
            selectedBook.setGenre(genreField.getValue());
            bookService.saveBook(selectedBook);
            refreshGrid();
            Notification.show("Book updated");
            clearForm();
        } else {
            BookEntity newBook = new BookEntity(titleField.getValue(), authorField.getValue(), genreField.getValue());
            bookService.saveBook(newBook);
            refreshGrid();
            Notification.show("Book saved");
            clearForm();
        }
    }

    /**
     * Logs out the current user.
     */
    private void logoutUser() {
        authenticationService.logout();
        VaadinSession.getCurrent().setAttribute(UserEntity.class, null);
        getUI().ifPresent(ui -> ui.navigate("/library/login"));
        Notification.show("Successfully logged out");
    }

    /**
     * Searches for books by title.
     */
    private void searchBook() {
        String title = searchBar.getValue();
        List<BookEntity> books = bookService.getBooksByTitle(title);
        grid.setItems(books);
        if (books.isEmpty()) {
            Notification.show("No books found");
        } else {
            Notification.show(books.size() + " books found");
        }
    }
}
