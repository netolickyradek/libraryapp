package com.library.view;

import com.library.data.entity.UserEntity;
import com.library.service.UserService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterListener;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Route("/library/register")
@Component
@Scope("prototype")
public class RegisterView extends VerticalLayout implements BeforeEnterListener {

    @Autowired
    public RegisterView(UserService userService) {
        Icon icon = VaadinIcon.BOOK.create();

        HorizontalLayout iconAndTitleLayout = new HorizontalLayout(icon, new H4("Library Reservation System"));
        iconAndTitleLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        iconAndTitleLayout.setWidthFull();

        Button backButton = new Button("Back to main page");
        backButton.addClickListener(event -> getUI().ifPresent(ui -> ui.navigate("/library")));
        HorizontalLayout backButtonLayout = new HorizontalLayout(backButton);
        backButtonLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        backButtonLayout.setWidthFull();

        HorizontalLayout headerLayout = new HorizontalLayout(iconAndTitleLayout, backButtonLayout);
        headerLayout.setWidthFull();
        headerLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        headerLayout.setAlignItems(FlexComponent.Alignment.CENTER);

        add(headerLayout);

        H1 title = new H1("Sign up");
        TextField usernameField = new TextField();
        usernameField.setLabel("Username");
        PasswordField passwordField = new PasswordField();
        passwordField.setLabel("Password");
        PasswordField confirmPasswordField = new PasswordField();
        confirmPasswordField.setLabel("Confirm Password");

        Button registerButton = getRegisterButton(userService, usernameField, passwordField, confirmPasswordField);

        FormLayout formLayout = new FormLayout();
        formLayout.add(title, usernameField, passwordField, confirmPasswordField, registerButton);
        formLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1));
        formLayout.setMaxWidth("400px");
        formLayout.getStyle().set("margin", "auto");

        setAlignItems(Alignment.CENTER);
        setSizeFull();
        add(formLayout);
    }

    private Button getRegisterButton(UserService userService, TextField usernameField, PasswordField passwordField, PasswordField confirmPasswordField) {
        Button registerButton = new Button("Sign up");
        registerButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        registerButton.addClickListener(event -> {
            String username = usernameField.getValue();
            String password = passwordField.getValue();
            String confirmPassword = confirmPasswordField.getValue();

            if (!password.equals(confirmPassword)) {
                Notification.show("Passwords do not match");
                return;
            }
            if (!isStrongPassword(password)) {
                Notification.show("Password must be at least 6 characters long, contain at least one uppercase letter and one number");
                return;
            }
            UserEntity user = userService.registerUser(username, password);
            if (user != null) {
                Notification.show("Registration successful");
                getUI().ifPresent(ui -> ui.navigate("/library/login"));
            } else {
                Notification.show("Registration failed");
            }
        });
        return registerButton;
    }

    public static boolean isStrongPassword(String password) {
        if (password.length() < 6) {
            return false;
        }
        boolean hasUpperCase = false;
        boolean hasDigit = false;
        for (char c : password.toCharArray()) {
            if (Character.isUpperCase(c)) {
                hasUpperCase = true;
            }
            if (Character.isDigit(c)) {
                hasDigit = true;
            }
        }
        return hasUpperCase && hasDigit;
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if (VaadinSession.getCurrent().getAttribute(UserEntity.class) != null) {
            event.rerouteTo(MainView.class);
        }
    }
}
