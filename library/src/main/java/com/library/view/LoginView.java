package com.library.view;

import com.library.data.entity.UserEntity;
import com.library.service.UserService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterListener;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Route("/library/login")
@Component
@Scope("prototype")
public class LoginView extends VerticalLayout implements BeforeEnterListener {

    @Autowired
    public LoginView(UserService userService) {
        Icon icon = VaadinIcon.BOOK.create();

        HorizontalLayout iconAndTitleLayout = new HorizontalLayout(icon, new H4("Library Reservation System"));
        iconAndTitleLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        iconAndTitleLayout.setWidthFull();

        Button backButton = new Button("Back to main page");
        backButton.addClickListener(event -> getUI().ifPresent(ui -> ui.navigate("/library")));
        HorizontalLayout backButtonLayout = new HorizontalLayout(backButton);
        backButtonLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        backButtonLayout.setWidthFull();

        HorizontalLayout headerLayout = new HorizontalLayout(iconAndTitleLayout, backButtonLayout);
        headerLayout.setWidthFull();
        headerLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        headerLayout.setAlignItems(FlexComponent.Alignment.CENTER);

        add(headerLayout);

        H1 title = new H1("Log in");
        TextField usernameField = new TextField();
        usernameField.setLabel("Username");
        PasswordField passwordField = new PasswordField();
        passwordField.setLabel("Password");

        FormLayout formLayout = new FormLayout();
        Button loginButton = getLoginButton(userService, usernameField, passwordField);
        Button registerButton = new Button("Don't have an account? Sign up!");
        registerButton.addClickListener(event -> getUI().ifPresent(ui -> ui.navigate("/library/register")));
        formLayout.add(title, usernameField, passwordField, loginButton, registerButton);
        formLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1));
        formLayout.setMaxWidth("400px");
        formLayout.getStyle().set("margin", "auto");
        add(formLayout);

        setAlignItems(Alignment.CENTER);
        setSizeFull();
    }

    private Button getLoginButton(UserService userService, TextField usernameField, PasswordField passwordField) {
        Button loginButton = new Button("Log in");
        loginButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        loginButton.addClickListener(event -> {
            String username = usernameField.getValue();
            String password = passwordField.getValue();
            UserEntity user = userService.loginUser(username, password);
            if (user != null) {
                VaadinSession.getCurrent().setAttribute(UserEntity.class, user);
                Notification.show("Log in successful");
                getUI().ifPresent(ui -> ui.navigate("/library"));
            } else {
                Notification.show("Log in failed");
            }
        });
        return loginButton;
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if (VaadinSession.getCurrent().getAttribute(UserEntity.class) != null) {
            event.rerouteTo(MainView.class);
        }
    }
}
